#import "CSAlertView.h"

extern "C" {
    BOOL _UIApplicationUsesLegacyUI();
}

const char *kCSAlertViewKey;

%group tweak
%hook UIAlertView
- (id) initWithTitle:(NSString  *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles {
    self = %orig;
    CSAlertView *alert = [[CSAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles];
    objc_setAssociatedObject(self, &kCSAlertViewKey, alert, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    return self;
 }

- (void)setDelegate:(id)delegate {
    %orig;
    CSAlertView *alert = objc_getAssociatedObject(self, &kCSAlertViewKey);
    [alert setDelegate:delegate];
}

-(void) show {
    CSAlertView *alert = objc_getAssociatedObject(self, &kCSAlertViewKey);
    [alert show];
}

-(void) dismissWithClickedButtonIndex:(int)index animated:(BOOL)animated {
    CSAlertView *alert = objc_getAssociatedObject(self, &kCSAlertViewKey);
    [alert dismissWithClickedButtonIndex:index animated:animated];
}

- (NSInteger)numberOfButtons {
    CSAlertView *alert = objc_getAssociatedObject(self, &kCSAlertViewKey);
    return [alert numberOfButtons];
}

- (BOOL)visible {
    CSAlertView *alert = objc_getAssociatedObject(self, &kCSAlertViewKey);
    return [alert visible];
}

- (NSString *)buttonTitleAtIndex:(NSInteger)buttonIndex {
    CSAlertView *alert = objc_getAssociatedObject(self, &kCSAlertViewKey);
    return [alert buttonTitleAtIndex:buttonIndex];
}

- (NSString *)title {
    CSAlertView *alert = objc_getAssociatedObject(self, &kCSAlertViewKey);
    return [alert title];
}

- (void)setTitle:(NSString *)title {
    CSAlertView *alert = objc_getAssociatedObject(self, &kCSAlertViewKey);
    return [alert setTitle:title];
}

%end
%end

%ctor {
    if (_UIApplicationUsesLegacyUI())
        %init(tweak);
}