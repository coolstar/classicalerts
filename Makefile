include theos/makefiles/common.mk

TWEAK_NAME = XXXClassicAlerts
XXXClassicAlerts_FILES = Tweak.xm CSAlertView.m
XXXClassicAlerts_FRAMEWORKS = UIKit CoreGraphics
include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 SpringBoard"
