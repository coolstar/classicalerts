//
//  CSAlertView.m
//  CSAlertViewTest
//
//  Created by CoolStar Org. on 4/15/14.
//  Copyright (c) 2014 CoolStar. All rights reserved.
//

#import "CSAlertView.h"

@interface UIImage (Framework)
+ (UIImage *)kitImageNamed:(NSString *)name;
@end

@implementation CSAlertView

- (id)initWithTitle:(NSString  *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... {
    NSArray *otherButtonTitlesArray = [NSArray arrayWithObjects:otherButtonTitles, nil];
    CGSize textSize = [message sizeWithFont:[UIFont fontWithName:@"Helvetica" size:18.0f] constrainedToSize:CGSizeMake(260, INT_MAX)];
    CGFloat textHeight = textSize.height;
    if (textHeight > 0)
        textHeight += 20;
    
    CGFloat buttonHeight = 0;
    if (cancelButtonTitle != nil || otherButtonTitlesArray.count > 0){
        buttonHeight = 43;
    }
    
    CGRect frame = CGRectMake(0,0,310,60 + textHeight + buttonHeight);
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _darkenView = [[UIImageView alloc] initWithImage:[UIImage kitImageNamed:@"UIPopupAlertListShadow"]];
        _darkenView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        _backgroundView = [[UIImageView alloc] initWithFrame:self.bounds];
        //_backgroundView.backgroundColor = [UIColor clearColor];
        _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [_backgroundView setImage:[[UIImage kitImageNamed:@"UIPopupAlertSheetBackground"] resizableImageWithCapInsets:UIEdgeInsetsMake(31, 284, 30, 283)]];
        [self addSubview:[_backgroundView autorelease]];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 18, 260, 22)];
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.numberOfLines = 0;
        [_titleLabel setText:title];
        [_titleLabel setFont:[UIFont fontWithName:@"Helvetica Bold" size:20.0f]];
        [_titleLabel setTextColor:[UIColor whiteColor]];
        [_titleLabel setShadowColor:[UIColor blackColor]];
        [_titleLabel setShadowOffset:CGSizeMake(0, -1)];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:[_titleLabel autorelease]];
        
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 50, 260, textSize.height)];
        _textLabel.numberOfLines = 0;
        _textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _textLabel.textAlignment = NSTextAlignmentCenter;
        [_textLabel setText:message];
        [_textLabel setFont:[UIFont fontWithName:@"Helvetica" size:18.0f]];
        [_textLabel setTextColor:[UIColor whiteColor]];
        [_textLabel setShadowColor:[UIColor blackColor]];
        [_textLabel setShadowOffset:CGSizeMake(0, -1)];
        [_textLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_textLabel];
        
        NSInteger buttonsCount = otherButtonTitlesArray.count;
        if (cancelButtonTitle != nil)
            buttonsCount += 1;
        
        CGFloat buttonWidth = 260.0f/(float)buttonsCount;
        
        _buttons = [[NSMutableArray alloc] init];
        
        NSInteger x = 13;
        NSInteger y = 50 + textSize.height;
        if (textSize.height > 0)
            y += 15;
        if (cancelButtonTitle != nil){
            UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(x, y, buttonWidth-6, 43)];
            [cancelButton setBackgroundImage:[[UIImage kitImageNamed:@"UIPopupAlertSheetDefaultButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 5, 21, 5)] forState:UIControlStateNormal];
            [cancelButton setBackgroundImage:[[UIImage kitImageNamed:@"UIPopupAlertSheetButtonPress"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 5, 21, 5)] forState:UIControlStateHighlighted];
            [cancelButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica Bold" size:20.0f]];
            [cancelButton.titleLabel setShadowColor:[UIColor blackColor]];
            [cancelButton.titleLabel setShadowOffset:CGSizeMake(0, -1)];
            [cancelButton setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [cancelButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:[cancelButton autorelease]];
            [_buttons addObject:cancelButton];
            x += buttonWidth+3;
        }
        for (NSString *buttonTitle in otherButtonTitlesArray){
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(x, y, buttonWidth-6, 43)];
            [button setBackgroundImage:[[UIImage kitImageNamed:@"UIPopupAlertSheetButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 5, 21, 5)] forState:UIControlStateNormal];
            [button setBackgroundImage:[[UIImage kitImageNamed:@"UIPopupAlertSheetButtonPress"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 5, 21, 5)] forState:UIControlStateHighlighted];
            [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica Bold" size:20.0f]];
            [button.titleLabel setShadowColor:[UIColor blackColor]];
            [button.titleLabel setShadowOffset:CGSizeMake(0, -1)];
            [button setTitle:buttonTitle forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:[button autorelease]];
            [_buttons addObject:button];
            x += buttonWidth + 3;
        }
        // Initialization code
    }
    return self;
}

- (void)show {
    if ([_delegate respondsToSelector:@selector(willPresentAlertView:)])
        [_delegate willPresentAlertView:_alertViewObject];
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    if (!keyWindow){
        keyWindow = [[UIApplication sharedApplication] windows][0];
    }
    UIView *rootView = nil;
    if (keyWindow.rootViewController){
        rootView = keyWindow.rootViewController.view;
    } else {
        rootView = keyWindow.subviews[0];
    }
    _darkenView.frame = rootView.bounds;
    self.frame = CGRectMake((rootView.bounds.size.width-self.frame.size.width)/2.0f + 13.0f, (rootView.bounds.size.height-self.frame.size.height)/2.0f, self.frame.size.width, self.frame.size.height);
    
    _darkenView.alpha = 0;
    self.alpha = 0;
    self.transform = CGAffineTransformMakeScale(0.5, 0.5);
    [UIView animateWithDuration:0.15 animations:^{
        self.transform = CGAffineTransformMakeScale(1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.15 animations:^{
            self.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    }];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
        _darkenView.alpha = 1;
    }];
    
    [rootView addSubview:_darkenView];
    [rootView addSubview:self];
    if ([_delegate respondsToSelector:@selector(didPresentAlertView:)])
        [_delegate didPresentAlertView:_alertViewObject];
}

- (NSString *)title {
    return _titleLabel.text;
}

- (void)setTitle:(NSString *)title {
    [_titleLabel setText:title];
}

- (NSInteger)numberOfButtons {
    return [_buttons count];
}

- (BOOL)visible {
    return (self.alpha != 0 && self.superview != nil);
}

- (NSString *)buttonTitleAtIndex:(NSInteger)buttonIndex {
    UIButton *button = [_buttons objectAtIndex:buttonIndex];
    return [button titleForState:UIControlStateNormal];
}

- (void)buttonClicked:(UIButton *)button {
    NSInteger buttonIndex = [_buttons indexOfObject:button];
    if (buttonIndex != -1)
        [self dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

- (void)dismissWithClickedButtonIndex:(int)index animated:(BOOL)animated {
    if ([_delegate respondsToSelector:@selector(alertView:willDismissWithButtonIndex:)])
        [_delegate alertView:_alertViewObject willDismissWithButtonIndex:index];
    [UIView animateWithDuration:animated?0.3:0.0 animations:^{
        self.alpha = 0;
        _darkenView.alpha = 0;
    } completion:^(BOOL finished) {
        if ([_delegate respondsToSelector:@selector(alertView:didDismissWithButtonIndex:)])
            [_delegate alertView:_alertViewObject didDismissWithButtonIndex:index];
        [_darkenView removeFromSuperview];
        [self removeFromSuperview];
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc {
    [_darkenView release];
    [_buttons release];
    [super dealloc];
}

@end