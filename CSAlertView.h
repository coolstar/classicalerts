//
//  CSAlertView.h
//  CSAlertViewTest
//
//  Created by CoolStar Org. on 4/15/14.
//  Copyright (c) 2014 CoolStar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSAlertView : UIView {
    NSString *_title, *_message;
    UIImageView *_backgroundView;
    UIImageView *_darkenView;
    UILabel *_titleLabel;
    UILabel *_textLabel;
    NSObject<UIAlertViewDelegate> *_delegate;
    UIAlertView *_alertViewObject;
    NSMutableArray *_buttons;
}

@property (nonatomic, retain) NSObject<UIAlertViewDelegate>* delegate;
@property (nonatomic, retain) UIAlertView *alertViewObject;
@property (nonatomic, copy) NSString *title;

- (id)initWithTitle:(NSString  *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;
- (void)show;
- (void)dismissWithClickedButtonIndex:(int)index animated:(BOOL)animated;
- (NSInteger)numberOfButtons;
- (BOOL)visible;
- (NSString *)buttonTitleAtIndex:(NSInteger)buttonIndex;

@end
